$(document).ready(function(){
  $("#submit").click(function(){
    var city = $("#city").val();
    var key = "e933d7d1fa38657893a72f8b519b7752";
    
    $.ajax({
      url: 'https://api.openweathermap.org/data/2.5/weather',
      dataType: 'json',
      type: 'GET',
      data: {q:city, appid: key, units: 'imperial'},
      
      success: function(data){
        console.log(data);
        var name = data.name;
        var country = data.sys.country;
        var temp = Math.round(data.main.temp);
        var humidity = data.main.humidity;
        var description = data.weather[0].description;
        var iconCode = data.weather[0].icon;
        var speed = data.wind.speed;
       
        
        $("#name").html(name + ", " + country);
        $("#temp").html(temp + "&#8457");
        $("#description").html(description);
        $("#humidity").html("Humidity:" + humidity + "%");
        $("#icon").html("<img src='https://openweathermap.org/img/w/" + iconCode + ".png' alt='current weather'>");
        $("#wind").html("Windspeed: " + speed);
        $("#city").val("");
      }
    });  
  }); 




  $("#check").click(function(){
    var currency = $("#currency").val();
    var apikey = "6b1c9b60cdcb4dc2f9403ff25f81d020";
    var endpoint = "latest";
    
    
    $.ajax({
      url: 'http://data.fixer.io/api/' + endpoint + '?access_key=' + apikey + '&symbol=' + currency,
      dataType: 'json',
      type: 'GET',
      success:
      function(data){
        var money = data.symbols;
        $("#money").html(money);
        console.log(money);
        $("#currency").val("");
      }
    });  
  }); 
});